
module.exports = (server) => {
  const io = require('socket.io')(server, {
    pingTimeout: 15000,
    pingInterval: 15000
  });

  const Lobby = require('./lobby')(io);
}
