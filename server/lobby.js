const secureRandom = require('secure-random');
const Game = require('./game');

const LOBBY_MAP = {};
const LOBBY_PLAYERS = {};
const sId = Symbol();
const sRoomName = Symbol();

module.exports = (io) => {
  class Lobby {
    static get all() {
      return LOBBY_MAP;
    }

    static get players() {
      return LOBBY_PLAYERS;
    }

    static update(socket) {
      (socket || Lobby.io).emit('lobbies', Object.values(Lobby.all).map(game => game.hash));
    }

    constructor() {
      this[sId] = secureRandom(16, { type: 'Buffer' }).toString('hex');
      this[sRoomName] = `game:${ this.id }`;
      this.register();

      if (this.game) this.game.update();
    }

    get id() {
      return this[sId];
    }

    get room() {
      return Lobby.io.adapter.rooms[this.roomName];
    }

    get roomName() {
      return this[sRoomName];
    }

    get players() {
      return this.room ? Object.keys(this.room.sockets).map(socket => Lobby.io.connected[socket]) : [];
    }

    get hash() {
      return {
        id: this.id,
        roomName: this.roomName,
        players: this.players.map(player => Lobby.players[player.id] || player.id)
      };
    }

    start(options) {
      options = Object.assign(options, {
        players: this.players.map((player) => {
          return {
            id: player.id,
            username: Lobby.players[player.id]
          }
        }),
        ioRoom: Lobby.io.in(this.roomName)
      });

      this.game = new Game(options);
    }

    register() {
      Lobby.all[this.id] = this;
      Lobby.update();
    }

    deregister() {
      if (this.room) return;
      delete Lobby.all[this.id];
    }

    join(socket, error) {
      if (this.players.length >= 2) return error('Lobby is full');
      socket.join(this.roomName, () => {
        error();
        Lobby.update();
        socket.once('disconnect', this.leave.bind(this, socket));
      });
    }

    leave(socket) {
      socket.leave(this.roomName, () => {
        this.deregister();
        Lobby.update();
      });
    }
  }

  Lobby.io = io.of('/lobby');

  Lobby.io.on('connect', (socket)  => {
    Lobby.update(socket);

    socket.on('username', (username) => {
      Lobby.players[socket.id] = username;
      Lobby.update();
    });

    socket.on('disconnect', () => {
      delete Lobby.players[socket.id];
    });

    socket.on('create', (ack) => {
      let game = new Lobby();
      ack(game.id);
    });

    socket.on('join', (id, error) => {
      let lobby = Lobby.all[id];
      if (lobby) lobby.join(socket, error);
      else error('Cannot find Lobby');
    });

    socket.on('leave', (id) => {
      let lobby = Lobby.all[id];
      if (lobby) lobby.leave(socket);
    });

    socket.on('start', (id, options, error) => {
      let lobby = Lobby.all[id];

      try {
        if (lobby) lobby.start(options);
        else error('Cannot find Lobby');
      } catch(err) {
        error(err);
      }
    });

    socket.on('move', (id, row, column, error) => {
      let lobby = Lobby.all[id];

      try {
        if (lobby && lobby.game) lobby.game.move(socket, row, column);
        else error('Cannot find Game');
      } catch(err) {
        error(err);
      }
    })
  });

  return Lobby;
};
