class Game {
  constructor({ row = 10, column = 10, players, ioRoom }) {
    if (players.length != 2) throw 'Game must have exactly 2 players';

    this.currentPlayerIndex = 0;
    this.lastMove = null;
    this.ioRoom = ioRoom;

    this.players = players.map((player, index) => {
      return {
        id: player.id,
        username: player.username,
        symbol: index % 2 ? 'O' : 'X'
      }
    });

    this.board = Array(row);

    for (let i = 0;i < row; i++){
      this.board[i] = Array(column).fill(null);
    };

    this.update();
  }

  get currentPlayer() {
    return this.players[this.currentPlayerIndex]
  };

  get hash() {
    return {
      board: this.board,
      lastMove: this.lastMove,
      winner: this.winner,
      players: this.players,
      currentPlayer: this.currentPlayer
    }
  }

  switchPlayer() {
    this.currentPlayerIndex = 1 - this.currentPlayerIndex;
  }

  move(socket, row, column) {
    if (this.winner) throw 'Game ended';
    if (this.board[row][column] !== null) throw 'Invalid position';
    if (socket.id != this.currentPlayer.id) throw `This is not your turn`;

    this.board[row][column] = this.currentPlayer.symbol;
    this.lastMove = [row, column];
    this.switchPlayer();
    this.update();
  }

  update() {
    this.ioRoom.emit('game', this.hash);
  }

  get winner() {
    const fetch = function (array, x, y, field, def) {
      if (array[x] && array[x][y])
        return array[x][y][field];

      return def;
    }

    for (let player of this.players) {
      let counter = new Array(this.board.length);

      for (let i = 0; i < counter.length; i++) {
        counter[i] = Array(this.board[0].length).fill({
          row: 0,
          col: 0,
          diag1: 0,
          diag2: 0
        });
      }

      for (let i = 0; i < counter.length; i++) {
        for (let j = 0; j < counter[i].length; j++) {
          if (this.board[i][j] == player.symbol) {
            counter[i][j] = {
              row: fetch(counter, i, j - 1, 'row', 0) + 1,
              col: fetch(counter, i - 1, j, 'col', 0) + 1,
              diag1: fetch(counter, i - 1, j - 1, 'diag1', 0) + 1,
              diag2: fetch(counter, i - 1, j + 1, 'diag2', 0) + 1
            };

            if (Object.values(counter[i][j]).find(k => k >= 5)) return player;
          }
        }
      }
    }
  }
}

module.exports = Game;
