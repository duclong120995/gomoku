import React from 'react';
import { Card, CardHeader, CardActions, CardText } from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';


import Game from 'game';

export default class Lobby extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: null,
      game: null,
      options: {
        row: 10,
        column: 10
      }
    };

    this.start = this.start.bind(this);
    this.leave = this.leave.bind(this);
    this.handleGame = this.handleGame.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  start() {
    this.ioLobby.emit('start', this.state.id, this.state.options, window.alert.bind(window));
  }

  leave() {
    this.props.history.push('/lobbies');
  }

  handleGame(game) {
    this.setState({ game: game })
  }

  handleChange(field, event) {
    let options = Object.assign({}, this.state.options);
    options[field] = parseInt(event.target.value);
    this.setState({ options: options })
  }

  handleClick(row, column) {
    this.ioLobby.emit('move', this.state.id, row, column, window.alert.bind(window));
  }

  componentDidMount() {
    const id = this.props.match.params.id;

    this.ioLobby = this.props.ioLobby;
    this.ioLobby.emit('join', id, (error) => {
      if (error) {
        window.alert(error);
        this.leave();
      } else
        this.setState({ id: id });
    });

    this.ioLobby.on('game', this.handleGame);
  }

  componentWillUnmount() {
    if (this.state.id) this.ioLobby.emit('leave', this.state.id);
    this.ioLobby.off('game', this.handleGame);
  }

  render() {
    return (
      <Card>
        <CardHeader
          title={ `Lobby: ${ this.state.id }` }
          subtitle={ this.props.lobby ? `Player: ${ this.props.lobby.players.join(' v.s. ') }` : null }
        />
        <CardText>
          {
            (this.state.game == null) && <div>
              Board size:
              <TextField
                name='options-row'
                type='number' style={ { width: '40px' } }
                value={ this.state.options.row }
                onChange={ this.handleChange.bind(this, 'row') }
              />
              x
              <TextField
                name='options-column'
                type='number' style={ { width: '40px' } }
                value={ this.state.options.column }
                onChange={ this.handleChange.bind(this, 'column') }
              />
            </div>
          }
          {
            (this.state.game != null) && <Game
              id={ this.ioLobby.id }
              game={ this.state.game }
              handleClick={ this.handleClick }
            />
          }
        </CardText>
        <CardActions>
          <FlatButton label='Start' primary={ true } onClick={ this.start }
            disabled={ (this.state.game != null) || (this.props.lobby && this.props.lobby.players.length != 2) }
          />
          <FlatButton label='Leave' secondary={ true } onClick={ this.leave } />
        </CardActions>
      </Card>
    )
  }
}
