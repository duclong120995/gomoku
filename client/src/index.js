import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Help from 'help'
import Lobbies from 'lobbies';
import './index.css';

function App() {
  return (
    <BrowserRouter>
      <MuiThemeProvider>
        <div className='app'>
          <AppBar title='Gomoku'>
            <div className='app-bar-children'>
              <FlatButton label='Home' href='/' />
              <FlatButton label='Lobbies' href='/lobbies' />
            </div>
          </AppBar>
          <div className='app-container'>
            <Route path='/lobbies' component={ Lobbies } />
            <Route exact path='/' component={ Help } />
          </div>
        </div>
      </MuiThemeProvider>
    </BrowserRouter>
  )
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
