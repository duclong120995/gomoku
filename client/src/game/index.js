import React from 'react';

export default function Game({ id, game, handleClick }) {
  return (
    <div>
      { game.winner && <b style={ { color: 'blue' } }>{ game.winner.username } win</b> }
      { game.winner == null && `Current player: ${ game.currentPlayer.symbol } - ${ game.currentPlayer.username }` }
      <Board game={ game } handleClick={ handleClick } />
    </div>
  )
}

function Board({ game: { board, lastMove }, handleClick }) {
  return (
    <table>
      <tbody>
        { 
          board.map((row, r) => {
            return (
              <tr key={ r } className={ lastMove && r === lastMove[0] ? 'last-row' : null}>
                {
                  row.map((cell, c) => {
                    return (
                      <td key={ c } className={ lastMove && c === lastMove[1] ? 'last-column' : null }>
                        <Square value={ cell } onClick={ handleClick.bind(null, r, c) } />
                      </td>
                    )
                  })
                }
              </tr>
            )
          })
        }
      </tbody>
    </table>
  )
}

function Square({ value, onClick: handleClick }) {
  return <button className='square' onClick={ handleClick }>{ value || ' ' }</button>;
}
