import React from 'react';

import { Card, CardHeader, CardText } from 'material-ui/Card';

export default function Help() {
  return (
    <Card>
      <CardHeader title='Help' />
      <CardText>
        Make a row of 5 to win.
      </CardText>
    </Card>
  )
}
