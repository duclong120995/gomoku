import React from 'react';
import { Route } from 'react-router-dom';
import io from 'socket.io-client';
import { Card, CardHeader, CardActions } from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';

import Lobby from 'lobby';

export default class Lobbies extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      lobbies: [],
      username: localStorage.getItem('username')
    };

    this.ioLobby = io('/lobby');
    this.changeUsername = this.changeUsername.bind(this);
    this.submitUsername = this.submitUsername.bind(this);
    this.create = this.create.bind(this);
    this.join = this.join.bind(this);
  }

  componentDidMount() {
    this.ioLobby.on('connect', this.submitUsername);
    this.ioLobby.on('lobbies', (lobbies) => {
      this.setState({
        lobbies: lobbies
      });
    });
  }

  changeUsername(event) {
    this.setState({
      username: event.target.value,
      canSubmit: true
    });
  }

  submitUsername(event) {
    localStorage.setItem('username', this.state.username);
    this.ioLobby.emit('username', this.state.username);
    this.setState({
      canSubmit: false,
      submitted: true
    });
    if (event) event.preventDefault();
  }

  create() {
    this.ioLobby.emit('create', this.join);
  }

  join(id) {
    this.props.history.push(`/lobbies/${ id }`);
  }

  render() {
    let lobbies = this.state.lobbies.map((lobby) => {
      return <LobbyItem key={ lobby.id } lobby={ lobby } onJoin={ this.join.bind(this, lobby.id) } />
    });

    return (
      <div className='lobbies'>
        <Route exact path='/lobbies' render={
          () => (
            <div>
              <Card>
                <form className='lobbies-username' onSubmit={ this.submitUsername }>
                  <div>
                    <TextField
                      floatingLabelText='Username'
                      errorText={ this.state.submitted && this.state.username ? null : 'Enter username before playing' }
                      value={ this.state.username }
                      onChange={ this.changeUsername }
                    />
                  </div>
                  <FlatButton label='Submit' primary={ true } onClick={ this.submitUsername } disabled={ !this.state.canSubmit } />
                </form>
              </Card>
              <FloatingActionButton className='lobbies-float-action' onClick={ this.create }>
                <ContentAdd />
              </FloatingActionButton>
              <div>
                { lobbies }
              </div>
            </div>
          )
        } />
        <Route
          path='/lobbies/:id'
          render={ (props) =>
            <Lobby
              {...props}
              key={ props.match.params.id }
              lobby={ this.state.lobbies.find((lobby) => lobby.id == props.match.params.id) }
              ioLobby={ this.ioLobby }
            />
          }
        />
      </div>
    );
  }
}

function LobbyItem({ lobby, onJoin: handleJoin }) {
  return (
    <Card className='lobbies-item'>
      <CardHeader
        title={ `Lobby: ${ lobby.id }` }
        subtitle={ `Player: ${ lobby.players.join(' v.s. ') }` }
      />
      <CardActions>
        <FlatButton label='Join lobby' primary={ true } onClick={ handleJoin } disabled={ lobby.players.length === 2 } />
      </CardActions>
    </Card>
  )
}
